# Instalación del proyecto

* Dependencias:

    - node>=16 ([Descarga v16.19.0](https://nodejs.org/download/release/v16.19.0/))
    - Expo ([Google play](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=es&gl=US&pli=1))

### Proceso de instalación:

Antes de ejecutar el proyecto por primera vez asegurate de instalar las dependencias utilizando el comando `npm install` desde una consola de comandos dentro del directorio del proyecto.

### Ejecución:

Para lanzar la aplicación abre una consola de comandos y ejecuta `npx expo start` o `npm start` esto lanzará el servidor de expo, una vez esté listo escanea el QR utilizando la aplicación de Expo en tu teléfono, (alternativamente puedes introducir manualmente la url proporcionada).

Una vez abierta la aplicación ya puedes modificar el código y se actualizará la aplicación de forma inmediata.