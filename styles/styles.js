import { StyleSheet } from 'react-native'
import color from './colors'

//Estilos para MainScreen
const mainStyles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: color.WHITE
    },

    containerCenter: {
        paddingTop: 10,
        alignItems: 'center',
        marginBottom: 25,
    },

    titleText: {
        fontSize: 28,
        marginTop: 20,
        color: color.BLUE,
        fontFamily: "normal"
    },

    btnMain: {
        width: 280,
        marginTop: 40,
        marginBottom: 20,
        alignItems: 'center',
        backgroundColor: color.BLUE,
        borderRadius: 60
    },

    btnTransparent: {
        backgroundColor: 'rgba(52, 52, 52, 0)',
        borderColor: color.BLUE,
        width: 280,
        borderWidth: 2,
        marginBottom: 20,
        borderRadius: 60
    },

    btntxt: {
        textAlign: 'center',
        fontSize: 17,
        color: color.WHITE,
        paddingVertical: 15,
        fontFamily: 'normal',
    },

    txtTransparent: {
        color: color.LIGHTPRIMARYCOLOR,
        fontSize: 14,
        fontFamily: 'normal',
    }
    
})

//Estilos para SplashScreen
const splashStyles = StyleSheet.create({
    image: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: color.WHITE,
    }
})

//Estilos para LoginScreen
const styles = StyleSheet.create({

    logo: {
        paddingTop: 10,
        alignItems: 'center',
    },
    btnVisibility:
    {
        height: 40,
        width: 35,
        paddingTop: 8,
        paddingLeft: 5,
        paddingRight: 5
    },
    checkBox: {
        marginLeft: 0,
        marginRight: 0,
        borderWidth: 0,
        backgroundColor: color.WHITE,
    },

    containerSocial: {
        paddingTop: 30,
        alignItems: 'center',
        marginBottom: 10,
    },

    buttonSocialIcon: {
        marginBottom: 10,
        width: 250,
        height: 60,
        alignItems: 'center',
        borderRadius: 60,
    },

    btnVisibility:
    {
        height: 40,
        width: 35,
        paddingTop: 8,
        paddingLeft: 5,
        paddingRight: 5
    },

    btnImage:
    {
        resizeMode: 'contain',
        height: '100%',
        width: '100%'
    },
    
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding:2
      }
})

//Estilos para ReservasScreen
const reservasStyles = StyleSheet.create({
    buttonReservas: {
        marginVertical: 20,
    },
    buttonImageIconStyle: {
        height: 30,
        width: 30
    },
    textInput: {
        borderColor: "gray",
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        textAlign: 'center',
        margin: 5
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 50,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      buttonAdd: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        backgroundColor:"#008000"
      },
      buttonCancel: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        backgroundColor:"#FF0000"
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        borderRadius: 5,
        padding: 15,
        elevation: 2,
        backgroundColor:"#FF0000"
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
})

export { mainStyles, splashStyles, styles, reservasStyles}