import React, { createContext, useReducer } from 'react'
import { saveUsuario, deleteUsuario, getUsuario} from '@storage/UsuarioAsyncStorage'

const initialState = {
    usuario: {
        nombre: '',
        apellido: '',
        email: '',
        password: ''
    },
    activo: false
}

 const usuarioReducer = async (state = initialState, payload) => {

    switch (payload.type) {

        case 'sign-in':
            const usuarioCorrecto =  await getUsuario(payload.data);
            return usuarioCorrecto 
        case 'sign-up':
            saveUsuario(payload.data);            
            return { ...state, usuario: payload.data, activo: true }
        case 'sign-out':
            deleteUsuario()
            return { ...state, usuario: payload.data, activo: false }
        default:
            return state
    }
}

const UsuarioContext = createContext(initialState)

function UsuarioProvider(props){

    const [login, loginAction] =  useReducer(usuarioReducer, initialState)

    return(
        <UsuarioContext.Provider value={[login, loginAction]}>
            {props.children}
        </UsuarioContext.Provider>
    )
}

export { UsuarioContext, UsuarioProvider}