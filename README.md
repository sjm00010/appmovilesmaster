# Aplicación móvil para el control horario de profesores
## Tabla de contenidos
1. [Descripción](#descripción)
2. [Visualización de la aplicación](#visualización-de-la-aplicación)
    - [Pantalla de Login](#login)
    - [Pantalla de Home](#pantalla-de-home)
    - [Pantalla de Reservas](#pantalla-de-reservas)
    - [Pantalla de Guardias](#pantalla-de-guardias)
3. [Enlaces](#enlaces)


### Descripción
El principal objetivo de este proyecto es el de crear un prototipo de una aplicación móvil mediante React Native, con la que los usuarios podrán realizar diferentes acciones:   
* Iniciar sesión en la aplicación.
* Realizar reservas de las instalaciones requeridas, para ello el usuario podrá seleccionará:
    * La instalación que se quiere reservar.
    * Los materiales que se quieren reservar.
    * La fecha en la que se quiere hacer la reserva.
    * La clase para la que se realiza la reserva.
* Registrar las guardias producidas por una ausencia de usuario:
    * El usuario podrá visualizar en una lista todas las guardias que ha registrado.
    * El usuario podrá registrar una nueva guardia, para ello deberá de incluir:
        * La fecha en la que se produce la guardia.
        * La clase a la que afecta la guardia.
        * Una breve descripción de las instrucciones a seguir por el profesorado que realice la guardia.
* Visualizar en la pantalla principal los eventos de reservas y guardias que hay en la base de datos.
    * Se podrá aplicar un filtro para separar los eventois segñun su tipo(Reserva o Guardia).
        
### Visualización de la aplicación

#### Login
<p align="center">
    <img src="https://drive.google.com/uc?export=view&id=1jS_CX-Rir3aWxvcBYSOsnR_NpLqXLH5i" width="200" height="400"/>    
</p>

Commo podemos oberservar en la imagen superior, se ha definido una pantalla de login sencilla en la que se debe de introducir un usuario(e-mail) y una constraseña.

#### Pantalla de Home
<p align="center">
    <img src="https://drive.google.com/uc?export=view&id=1jcT4HZpxTdcvHtWXihzp2esy0PqabE7P" width="200" height="400"/>
    <img src="https://drive.google.com/uc?export=view&id=1junZzXk8bhfSBqECEEh6LXTeZXkJaO7N" width="200" height="400"/>    
</p>

Respecto a la pantalla de Home de la aplicación, se ha implementado una lista scrollable en la que se muestren todas las reservas y guardias que tenga asignadas el usuario, los elementos están ordenadso desde la fecha más reciente.

Además, se ha implementado un filtro para mostrar únicamente el tipo de evento elegido por el usuario, pudiendo mostrar en la lista los eventos:
* Reservas
* Guardias
* Todos los eventos

Por otro lado, podemos apreciar como se ha implementado una nevegación mediante una barra de navegación que se puede visualizar en la parte baja de la pantalla, cuyos items son las secciones de Home, Añadir una nueva reserva y Añadir una nueva baja.
#### Pantalla de Reservas
<p align="center">
    <img src="https://drive.google.com/uc?export=view&id=1jt_69olsIwzzFdU_590bXMB410MhR7tR" width="200" height="400"/>
    <img src="https://drive.google.com/uc?export=view&id=1k0OOBjacvxR5sodqB5bggtzeAfdj13PP" width="200" height="400"/>    
    <img src="https://drive.google.com/uc?export=view&id=1jvZrzHOaGyOI1DN7Cafcdq-GzYhTL2Rb" width="200" height="400"/>    
    <img src="https://drive.google.com/uc?export=view&id=1jiGLyRktPHT5yiXu5FgrMWG29LNkqPsC" width="200" height="400"/>  
</p>

En la sección de añadir una nueva reserva, podemos ver que hemos implementado un formulario a rellenar por el usuario:

* Deberá de seleccionar el aula/pista que desea reservar.
* Tendrá la posisbiidad de seleccionar el material que quiera reservar, siempre y cuando esté disponible.
* Debe de seleccionar una fecha en la que la pista esté libre.
* Deberá de seleccionar la clase para la que se reserva el aula.

Por otro lado, cuando en la pantalla de Home se selecciona una reserva para modificarla, esta te redirige a la pantalla de reservas y te muestra los valores anteriores. Además te aparece el botón de eliminar reserva y te cmabia el de añadir reserva por el de modificar reserva.

#### Pantalla de Guardias
<p align="center">
    <img src="https://drive.google.com/uc?export=view&id=1jrVOuiwU1hUKctRaZfH9C05alGfEecYu" width="200" height="400"/>
    <img src="https://drive.google.com/uc?export=view&id=1joNKl4YoK1A_S_ET_mnKFfHMeEcTGliA" width="200" height="400"/>    
    <img src="https://drive.google.com/uc?export=view&id=1jaS9m9VojJZ55gMsmt64ErnOm63_gXX0" width="200" height="400"/>
    <img src="https://drive.google.com/uc?export=view&id=1jkosS5mnbBXTjIus1HWWysKIXCiCS10F" width="200" height="400"/>
</p>

Por último, en la pantalla de Guardias hemos implementado una lista en la que se muestran aquellas bajas que nosostros tenemos y deberán de ser asignadas a algún otro profesor.

En la parte superior de la pantalla se ha implementado un botón para crear una nueva baja que te redirige a una nueva pantalla en la que se muestra un formulario a rellenar:

* Se debe seleccionar la fecha en la que se va a producir la baja.
* Se debe de seleccionar la clase a la que afecta dicha baja.
* Se debe de dar una descripcion de cuales son las tareas que deberán de realizar los estudiantes o instrucciones al sustituo.

Por otro lado, para visualizar el contenido de una guardia producida por un compañero, bastará con ir a la pantalla Home y seleccionar una baja para que nos aparezca un modal donde se visualice el contenido.

### Enlaces
Enlaces de interés:
* GIT : [https://gitlab.com/sjm00010/appmovilesmaster](https://gitlab.com/sjm00010/appmovilesmaster)
* Jira : [https://flo00008.atlassian.net/jira/software/projects/APP/boards/1](https://flo00008.atlassian.net/jira/software/projects/APP/boards/1)