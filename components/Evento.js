import { Text, View, StyleSheet, Dimensions } from 'react-native';
import { Button, Card } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { getAula, getClase } from '@storage/DataSimulator';

function getIconType(tipo) {
    switch (tipo) {
        case 'Reserva':
            return "md-school-outline";
        case 'Guardia':
            return "md-medkit-outline";
        default:
            return "ios-bug"
    }
}

function Evento({ tipo, titulo, ubicacion, fecha, color }) {
    return (
        <Card containerStyle={{ width: Dimensions.get('window').width - 25, height: 100, borderColor: color }}>
            <View style={{ justifyContent: 'center', height: 60 }}>
                <View style={{ display: "flex", flexDirection: "row", alignItems: 'center' }}>
                    <Button
                        buttonStyle={styles.localize}
                        icon={<Ionicons name={getIconType(tipo)} size={28} style={{ alignSelf: 'center' }} />}
                    />
                    <Text style={{ fontSize: 18, textAlignVertical: 'center', fontWeight: 'bold', padding: 8 }}>{getClase(titulo)}</Text>
                    <View style={{ flexGrow: 1 }} />

                </View>
                <View
                    style={{
                        borderWidth: 0.5,
                        borderColor: 'grey',
                        marginTop: 8,
                    }}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 16, textAlignVertical: 'auto', color: "grey" }}>{getAula(ubicacion)}</Text>
                    <Text style={{ fontSize: 16, textAlignVertical: 'auto', color: "grey" }}>{fecha.format('YYYY-MM-DD')}</Text>
                </View>
            </View>
        </Card>
    )
}

const styles = StyleSheet.create({
    localize: {
        padding: 4,
        backgroundColor: 'white',
        borderColor: 'green',
        borderWidth: 2,
        borderRadius: 10,
        justifyContent: 'center'
    }
});

export default Evento;