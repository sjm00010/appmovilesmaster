import { View, Text, Alert, TextInput, Modal, Pressable, Button, TouchableOpacity, Image } from 'react-native';
import { SelectList } from 'react-native-dropdown-select-list';
import { reservasStyles, styles } from '@styles/styles';
import { useEffect, useState } from 'react';

export default function MaterialesPicker({ materialesPreSeleccionados, materialesDisponibles, onChange }) {
    const [modalMaterialesVisible, setModalMaterialesVisible] = useState(false);

    const [listaMateriales, setListaMateriales] = useState([]);
    const [materialesSeleccionados, setMaterialesSeleccionados] = useState(materialesPreSeleccionados ?? []);
    const [materialSeleccionado, setMaterialSeleccionado] = useState('');
    const [materialIdSeleccionado, setMaterialIdSeleccionado] = useState('');
    const [cantidadMaterialSeleccionado, setCantidadMaterialSeleccionado] = useState(1);

    useEffect(() => {
        const lista = materialesDisponibles
            .filter(m => m.disponibles >= 1)
            .sort((a, b) => a.nombre < b.nombre ? -1 : 1)
        setListaMateriales(lista);

    }, [materialesDisponibles]);

    useEffect(() => {
        if (materialesPreSeleccionados.length > 0)
            setMaterialesSeleccionados(materialesPreSeleccionados);
    }, [materialesPreSeleccionados]);

    useEffect(() => {
        if (materialIdSeleccionado != '' && materialIdSeleccionado != materialSeleccionado.id) {
            setMaterialSeleccionado(getMaterialById(materialIdSeleccionado));
            setMaterialIdSeleccionado('');
        }
    }, [materialIdSeleccionado]);

    useEffect(() => {
        if (listaMateriales[0] != undefined) setMaterialSeleccionado(listaMateriales[0]);
    }, [listaMateriales]);

    useEffect(() => { onChange(materialesSeleccionados) }, [materialesSeleccionados]);

    function addMaterial() {
        if (cantidadMaterialSeleccionado <= 0 || cantidadMaterialSeleccionado > materialSeleccionado.disponibles) {
            Alert.alert('Error en unidades de material seleccionada', 'Por favor, seleccione un número menor al disponible y mayor a 0');
            return;
        }

        const matIndex = listaMateriales.findIndex(m => m.id === materialSeleccionado.id);
        listaMateriales.splice(matIndex, 1);

        const newMat = { material: materialSeleccionado, cantidad: cantidadMaterialSeleccionado };
        const newMateriales = [...materialesSeleccionados, newMat];
        setMaterialesSeleccionados(newMateriales);

        close();
    }

    function getMaterialById(id) {
        return listaMateriales.find(m => m.id === id) ?? materialesSeleccionados.find(ms => ms.material.id === id);
    }

    const deleteMaterialHandler = (item) => {
        const matIndex = materialesSeleccionados.findIndex(ms => ms.material.id === item.id);
        materialesSeleccionados.splice(matIndex, 1);

        const newMateriales = [...materialesSeleccionados];
        setMaterialesSeleccionados(newMateriales);
    }

    function close() {
        setCantidadMaterialSeleccionado(1);
        setModalMaterialesVisible(false);
    }

    return (
        <View style={{ padding: 10 }}>
            <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Material</Text>
            <Button title="Añadir material" onPress={() => setModalMaterialesVisible(true)} />

            {materialesSeleccionados.map(({ material, cantidad }) => (
                <View style={styles.inputContainer} key={material.id}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'left' }}>{material.nombre}</Text>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'left' }}>{cantidad}</Text>

                    <TouchableOpacity onPress={() => deleteMaterialHandler(material)}>
                        <Image
                            style={reservasStyles.buttonImageIconStyle}
                            source={require('@assets/delete.jpg')}
                        />
                    </TouchableOpacity>
                </View>
            ))}

            <View style={reservasStyles.centeredView}>
                <Modal
                    animationType='slide'
                    transparent={true}
                    visible={modalMaterialesVisible}
                    onRequestClose={close}
                >
                    <View style={reservasStyles.centeredView}>
                        <View style={reservasStyles.modalView}>
                            <View style={styles.inputContainer}>
                                <SelectList
                                    setSelected={setMaterialIdSeleccionado}
                                    data={listaMateriales.map(m => ({ key: m.id, value: m.nombre }))}
                                    defaultOption={({ key: materialSeleccionado?.id, value: materialSeleccionado?.nombre })}
                                    placeholder="Material" />

                                <TextInput value={cantidadMaterialSeleccionado}
                                    defaultValue={cantidadMaterialSeleccionado.toString()}
                                    style={reservasStyles.textInput}
                                    onChangeText={num => setCantidadMaterialSeleccionado(Number(num))}
                                    numeric
                                    keyboardType='numeric'
                                />
                                <Text style={{ fontSize: 18, fontWeight: 'bold', alignItems: 'center' }}> / {materialSeleccionado?.disponibles ?? 0}</Text>
                            </View>
                            <View style={styles.inputContainer}>
                                <Pressable
                                    style={[reservasStyles.buttonAdd, styles.buttonClose]}
                                    onPress={addMaterial}
                                >
                                    <Text style={reservasStyles.textStyle}>Añadir Material</Text>
                                </Pressable>
                                <Pressable
                                    style={[reservasStyles.buttonCancel, styles.buttonClose]}
                                    onPress={close}
                                >
                                    <Text style={reservasStyles.textStyle}>Cancelar</Text>
                                </Pressable>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        </View>
    )
}