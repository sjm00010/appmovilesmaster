import { View, Text, Modal, Pressable } from 'react-native';
import { reservasStyles } from '@styles/styles';
import { getClase } from '@storage/DataSimulator';

export default function ModalResumenGuardia({ guardia, setGuardia }) {
    console.log(guardia)

    function close() {
        setGuardia(undefined);
    }

    return (
        <Modal
            animationType='slide'
            transparent={true}
            onRequestClose={close}
            style={reservasStyles.centeredView}
        >
            <View style={reservasStyles.centeredView}>
                <View style={reservasStyles.modalView}>
                    <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Resumen de la guardia</Text>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>{getClase(guardia.clase)}</Text>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>{guardia.fecha.format('DD/MM/YYYY')}</Text>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', textDecorationLine: 'underline' }}>Descripción</Text>
                    <Text style={{ fontSize: 16, textAlign: 'center'}}>{guardia.descripcion}</Text>
                </View>
                <View>
                    <Pressable
                        style={reservasStyles.buttonClose}
                        onPress={close}
                    >
                        <Text style={reservasStyles.textStyle}>Cerrar</Text>
                    </Pressable>
                </View>
            </View>
        </Modal>
    )
}