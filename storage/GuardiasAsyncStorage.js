import AsyncStorage from '@react-native-async-storage/async-storage';
import { getGuardias, getBajas } from '@storage/DataSimulator';
import moment from 'moment';
import 'moment/locale/es';

const STORAGE_KEY_GUARDIAS = 'guardias';
const STORAGE_KEY_BAJAS = 'bajas';

export async function saveBajas(data) {
    try {
        await AsyncStorage.setItem(STORAGE_KEY_BAJAS, JSON.stringify(data));
    } catch (error) {
        return 'Error de sintaxis'
    }
}

export async function getAllBajas() {
    try {
        const item = (await AsyncStorage.getItem(STORAGE_KEY_BAJAS)) ?? '[]';
        const bajas = JSON.parse(item)
        return bajas
            .map(b => {
                b.fecha = moment(b.fecha)
                return b;
            });

    } catch (error) {
        return getBajas();
    }
}

export async function getAllGuardias() {
    const guardias = getGuardias();

    return guardias
            .map(g => {
                g.fecha = moment(g.fecha)
                return g;
            });
}

export async function clearGuardias() {
    try {
        await AsyncStorage.setItem(STORAGE_KEY_GUARDIAS, '');
        await AsyncStorage.setItem(STORAGE_KEY_BAJAS, '');
    } catch (error) {
        // Error retrieving data
        console.log("Error al limiar la base de datos:" + error.message)
    }
}