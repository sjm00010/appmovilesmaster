
import { getClases } from '@storage/DataSimulator';

export async function getAllClases() {
    return getClases();
}