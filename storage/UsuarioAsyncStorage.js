import { getUser } from "@storage/DataSimulator";

export function isUserValid(user) {
    const dbUser = getUser();

    return user.email == dbUser.email && user.password == dbUser.password;
}