
import { getAulas } from '@storage/DataSimulator';

export async function getAllAulas() {
    return getAulas();
}