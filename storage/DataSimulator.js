import datos from '@storage/datos.json';
import { clearGuardias, saveBajas } from '@storage/GuardiasAsyncStorage';
import { clearReservas, saveReserva } from '@storage/ReservasAsyncStorage';

export const getUser = () => deepCopy(datos["user"]);
export const getReservas = () => deepCopy(datos["reservas"]);
export const getTiposEventos = () => deepCopy(datos["tiposEvento"]);
export const getMateriales = () => deepCopy(datos['materiales']);
export const getAulas = () => deepCopy(datos['aulas']);
export const getAula = (id) => {
    if(id == undefined) return "";
    return deepCopy(datos['aulas'].find(item => item.id === id)).nombre
};
export const getClases = () => deepCopy(datos['clases']);
export const getClase = (id) => {
    if(id == undefined) return "";
    return deepCopy(datos['clases'].find(item => item.id === id)).nombre
};
export const getGuardias = () => deepCopy(datos['guardias']);
export const getBajas = () => deepCopy(datos['bajas']);



function deepCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
}

export async function setupDummyData() {
    await clearGuardias();
    await clearReservas();

    await saveReserva(getReservas());
    await saveBajas(getBajas());
}
