import { getAllReservas } from '@storage/ReservasAsyncStorage';
import { getMateriales } from '@storage/DataSimulator';

async function getMaterialesReservados(dia) {
    return (await getAllReservas())
        .filter(reserva => (dia != undefined && dia.isSame(reserva.fecha, 'day')))
        .flatMap(reserva => reserva.materiales)
        .reduce((acc, curr) => {
            const idx = curr.material.id;

            acc[idx] = acc[idx] ?? { ...curr, cantidad: 0 };
            acc[idx].cantidad += curr.cantidad;

            return acc;
        }, {});
}

export async function getMaterialesDisponibles(dia) {
    const materialesReservados = await getMaterialesReservados(dia);
    const materiales = await getAllMateriales();

    return materiales
        .map(m => {
            m.disponibles = materialesReservados[m.id] != undefined ? (m.disponibles - materialesReservados[m.id].cantidad) : m.disponibles;
            return m;
        })
        .filter(m => m.disponibles > 0);
}

export async function haySuficientesMateriales(dia, listaMateriales) {
    const materialesReservados = await getMaterialesReservados(dia);

    return listaMateriales.every(item =>
        item.cantidad <= item.material.disponibles - (materialesReservados[item.material.id]?.cantidad ?? 0)
    );
}

export async function getAllMateriales() {
    return getMateriales();
}