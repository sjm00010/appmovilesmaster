import AsyncStorage from '@react-native-async-storage/async-storage';
import { getReservas, getTiposEventos } from '@storage/DataSimulator';
import moment from 'moment';
import 'moment/locale/es';

const STORAGE_KEY = 'reservas';

export async function saveReserva(data){
    try {
        await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(data));
    } catch (error) {
        return 'Error de sintaxis'
    }
}

export async function getAllReservas(){
    try {
        const item = (await AsyncStorage.getItem(STORAGE_KEY)) ?? '[]';
        const reservas = JSON.parse(item)
        return reservas.map(r => {
            r.fecha = moment(r.fecha)
            return r;
        });

    } catch (error) {
        return getReservas();
    }
}

export async function getTiposEvento() {
    return getTiposEventos();
}

export async function clearReservas(){
    try {
        AsyncStorage.setItem(STORAGE_KEY, '');
    } catch (error) {
        // Error retrieving data
        console.log("Error al limiar la base de datos:" + error.message)
    }
}
