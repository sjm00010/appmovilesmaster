import { useState, useEffect } from 'react';
import { View, Text, ScrollView, StatusBar, Button, Alert } from 'react-native';
import { SelectList } from 'react-native-dropdown-select-list';
import color from '@styles/colors';
import { reservasStyles } from '@styles/styles';
import { getAllClases } from '@storage/ClasesAsyncStorage';
import { getAllAulas } from '@storage/AulasAsyncStorage';
import CalendarPicker from 'react-native-calendar-picker';
import moment from 'moment';
import 'moment/locale/es';
import { saveReserva, getAllReservas } from '@storage/ReservasAsyncStorage';
import { haySuficientesMateriales, getMaterialesDisponibles, getAllMateriales } from '@storage/MaterialesAsyncStorage';
import { getAula, getClase } from '@storage/DataSimulator';
import uuid from 'react-native-uuid';
import { HomeScreenName } from '@screens/HomeScreen';
import MaterialesPicker from '@components/MaterialesPicker';

export const ReservasScreenName = 'Nueva reserva';

export function ReservasScreen({ route, navigation }) {
    const [reservaId, setReservaId] = useState(undefined);
    const [modificar, setModificar] = useState(false);
    const [selectedAulaId, setSelectedAulaId] = useState('');
    const [selectedClaseId, setSelectedClaseId] = useState('');
    const [selectedMateriales, setSelectedMateriales] = useState([]);
    const [selectedFecha, setSelectedFecha] = useState(fechaHoy());

    const [modalCalendarioVisible, setModalCalendarioVisible] = useState(false);
    const [isSelectedDate, setIsSelectedDate] = useState(false);
    const [disabledDays, setDisabledDays] = useState([]);

    const [listaMateriales, setListaMateriales] = useState([]);
    const [listaAulas, setListaAulas] = useState([]);
    const [listaClases, setListaClases] = useState([]);

    const [materialesDisponibles, setMaterialesDisponibles] = useState([]);
    useEffect(() => {
        setMaterialesDisponibles(listaMateriales.filter(m => !selectedMateriales.some(sm => sm.material.id == m.id)));
    }, [listaMateriales, selectedMateriales]);

    useEffect(() => {
        actualizarCantidadMaterialesDisponibles(selectedFecha);
    }, [selectedFecha]);

    useEffect(() => {
        calculateDisabledDays(selectedFecha);
    }, [selectedMateriales, selectedAulaId, selectedFecha]);

    useEffect(() => { fetchData(); }, []);

    async function actualizarCantidadMaterialesDisponibles(fecha) {
        const listaMaterialesDisponibles = await getMaterialesDisponibles(fecha);
        setListaMateriales(listaMaterialesDisponibles);

        const invalidos = selectedMateriales
            .map(sm => {
                const material = listaMaterialesDisponibles.find(a => a.id === sm.material.id);
                const reservados = sm.cantidad;
                const disponibles = material?.diponibles;
                return { material, reservados, disponibles };
            })
            .filter(r => r.reservados > listaMaterialesDisponibles);

        invalidos.forEach(i => Alert.alert('Error insuficientes materiales', `Has seleccionado ${i.reservados} ${i.material.nombre}, pero solo quedan ${i.disponibles}`));
    }

    function isDefaultData() {
        return route.params && route.params.hasOwnProperty('datos') && route.params.datos;
    }

    function loadData() {
        if (isDefaultData()) {
            setModificar(true);
            console.log(JSON.stringify(route.params.datos))

            setSelectedAulaId(route.params.datos.aula);
            setSelectedMateriales(route.params.datos.materiales);
            setSelectedFecha(route.params.datos.fecha);
            setSelectedClaseId(route.params.datos.clase);

            setReservaId(route.params.datos.id);

            navigation.setParams({
                datos: undefined,
            });
        }
    }

    useEffect(() => { loadData(); }, []);

    async function fetchData() {
        setListaMateriales(await getAllMateriales());
        setListaAulas(await getAllAulas());
        setListaClases(await getAllClases());
    }

    async function calculateDisabledDays(fecha) {
        const daysToCheck = getDiasDelMes(fecha);
        const invalidDays = await Promise.all(daysToCheck.map(async day => (await shouldDisableDay(day, selectedMateriales)) ? day : null));

        setDisabledDays(invalidDays.filter(i => i));
    }

    function getDiasDelMes(date) {
        const dias = [];
        const totalDias = date.daysInMonth();
        for (let dia = 1; dia <= totalDias; dia++) {
            dias.push(moment(date).date(dia));
        }

        return dias;
    }

    async function shouldDisableDay(dia, materiales) {
        return isWeekend(dia) || !(await haySuficientesMateriales(dia, materiales));
    }

    function fechaHoy() {
        return moment(new Date());
    }

    function isWeekend(date) {
        return date.day() == 0 || date.day() == 6;
    }

    function setFecha(fecha) {
        setSelectedFecha(fecha);
        setModalCalendarioVisible(false);
        setIsSelectedDate(true);
    }

    function renderDatePicker() {
        if (!modalCalendarioVisible) return;

        return (
            <View>
                <CalendarPicker
                    onDateChange={setFecha}
                    onMonthChange={calculateDisabledDays}
                    minDate={fechaHoy()}
                    initialDate={fechaHoy()}
                    weekdays={['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']}
                    months={[
                        'Enero',
                        'Febrero',
                        'Marzo',
                        'Abril',
                        'Mayo',
                        'Junio',
                        'Julio',
                        'Agosto',
                        'Septiembre',
                        'Octubre',
                        'Noviembre',
                        'Diciembre',
                    ]}
                    previousTitle="Anterior"
                    nextTitle="Siguiente"
                    disabledDates={disabledDays}
                    startFromMonday
                />
            </View>
        );
    };

    function getSelectedDate() {
        if (!isSelectedDate && selectedFecha == '') return;

        return (
            <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', marginTop: 20 }}>Fecha seleccionada: {selectedFecha.format('DD-MM-YYYY')}</Text>
        );
    }

    function onClickButtonFecha() {
        if (selectedAulaId == '') {
            Alert.alert('Error en el aula seleccionada', 'Por favor, introduzca un aula');
            return;
        }

        setModalCalendarioVisible(true);
    }

    function camposValidos() {
        if (selectedAulaId == '') {
            Alert.alert('Error en el aula seleccionada', 'Por favor, introduzca un aula')
            return false;
        }

        if (selectedClaseId == '') {
            Alert.alert('Error en la clase', 'Por favor, seleccione una clase');
            return false;
        }

        return true;
    }

    function limpiarCampos() {
        setSelectedAulaId('');
        setModalCalendarioVisible(false);
        setSelectedFecha(fechaHoy());
        setSelectedClaseId('');
    }

    async function addReserva() {
        if (!camposValidos()) return;

        const reserva = {
            id: uuid.v4(),
            tipo: 'Reserva',
            aula: selectedAulaId,
            materiales: selectedMateriales,
            fecha: selectedFecha,
            clase: selectedClaseId
        }

        const reservas = await getAllReservas();
        reservas.push(reserva);
        saveReserva(reservas);

        limpiarCampos();
        goToScreen(HomeScreenName);
    }

    function removeItem(arr, value) {
        let index = arr.findIndex(el => el.id == value);
        if (index > -1) {
            arr.splice(index, 1);
        }
        return arr;
    }

    async function borrarReserva() {
        if (!camposValidos()) return;       
        
        const reservas = removeItem(await getAllReservas(), reservaId);
        saveReserva([...reservas]);
        
        limpiarCampos();
        goToScreen(HomeScreenName);
    }

    async function modificarReserva() {
        if (!camposValidos()) return;

        const reserva = {
            id: reservaId,
            tipo: 'Reserva',
            aula: selectedAulaId,
            materiales: selectedMateriales,
            fecha: selectedFecha,
            clase: selectedClaseId
        }        
        
        const reservas = removeItem(await getAllReservas(), reserva.id);
        saveReserva([...reservas, reserva]);
        
        limpiarCampos();
        goToScreen(HomeScreenName);
    }

    return (
        <ScrollView
            keyboardDismissMode='on-drag'
            keyboardShouldPersistTaps='always'
            style={{ backgroundColor: color.WHITE }}>
            <StatusBar backgroundColor={color.BLUE} translucent={true} />
            <View style={{ padding: 10 }}>
                <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Aula</Text>
                {selectedAulaId != '' &&
                    <SelectList setSelected={setSelectedAulaId} defaultOption={{ key: selectedAulaId, value: getAula(selectedAulaId) }} data={listaAulas.map(a => ({ key: a.id, value: a.nombre }))} placeholder="Seleccione aula" />
                }
                {selectedAulaId == '' &&
                    <SelectList setSelected={setSelectedAulaId} data={listaAulas.map(a => ({ key: a.id, value: a.nombre }))} placeholder="Seleccione aula" />
                }

            </View>

            <MaterialesPicker materialesPreSeleccionados={selectedMateriales} materialesDisponibles={materialesDisponibles} onChange={setSelectedMateriales} />

            <View style={{ padding: 10, alignContent: 'center' }}>
                <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Fecha</Text>

                <Button
                    title="Seleccionar fecha"
                    onPress={() => setModalCalendarioVisible(!modalCalendarioVisible)}
                />
                {renderDatePicker()}

                {getSelectedDate()}
            </View>

            <View style={{ padding: 10 }}>
                <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Clase</Text>
                {selectedClaseId != '' &&
                    <SelectList setSelected={setSelectedClaseId} defaultOption={{ key: selectedClaseId, value: selectedClaseId }} data={listaClases.map(c => c.id)} placeholder="Seleccione la clase" />
                }
                {selectedClaseId == '' &&
                    <SelectList setSelected={setSelectedClaseId} data={listaClases.map(c => c.id)} placeholder="Seleccione la clase" />
                }
            </View>
            <View style={{ padding: 10, alignContent: 'center' }}>
                {modificar &&
                    <Button
                        title="Modificar reserva"
                        style={reservasStyles.buttonReservas}
                        color={color.GREEN}
                        onPress={() => modificarReserva()}
                    />
                }
                {modificar &&
                    <View style={{ paddingTop: 20, alignContent: 'center' }}>
                        <Button
                            title="Borrar reserva"
                            style={reservasStyles.buttonReservas}
                            color={color.RED}
                            onPress={() => borrarReserva()}
                        />
                    </View>
                }

                {!modificar &&
                    <Button
                        title="Reservar"
                        style={reservasStyles.buttonReservas}
                        color={color.GREEN}
                        onPress={() => addReserva()}
                    />
                }
            </View>

            <View style={{ padding: 10, alignContent: 'center' }}>
                <Button
                    title="Cancelar"
                    style={reservasStyles.buttonReservas}
                    color={color.GREY}
                    onPress={() => {
                        limpiarCampos();
                        goToScreen(HomeScreenName);
                    }}
                />
            </View>
        </ScrollView>

    );

    function goToScreen(routeName) {
        navigation.navigate(routeName)
    }
}
