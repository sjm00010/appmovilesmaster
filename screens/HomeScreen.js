import { useState, useEffect } from 'react';
import { TouchableOpacity, ScrollView, View } from 'react-native';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    renderers
} from 'react-native-popup-menu';
import { Ionicons } from '@expo/vector-icons';

import Evento from '@components/Evento';
import ModalResumenGuardia from '@components/ModalResumenGuardia';

import { getAllReservas } from '@storage/ReservasAsyncStorage'
import { getAllGuardias } from '@storage/GuardiasAsyncStorage'
import { getTiposEventos } from '@storage/DataSimulator'
import { ReservasScreenName } from './ReservasScreen';

import moment from 'moment';
import 'moment/locale/es';

const { SlideInMenu } = renderers;

export const HomeScreenName = 'Inicio';

export function HomeScreen({ navigation }) {
    const [typeSelected, setTypeSelected] = useState('All');
    const [listaEventos, setListaEventos] = useState([]);
    const [eventoSelected, setEventoSelected] = useState(undefined);
    const [tiposEvento] = useState(getTiposEventos());

    useEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <Filtro />
            )
        })
    }, [navigation, typeSelected]);

    const Filtro = () => {
        return (
            <Menu renderer={SlideInMenu} onSelect={value => {
                setTypeSelected(value)
            }}>
                <MenuTrigger style={{ marginRight: 20, padding: 7 }}>
                    <Ionicons name="ios-funnel" size={20} />
                </MenuTrigger>
                <MenuOptions>
                    {tiposEvento.map((tipo) => (
                        <MenuOption key={tipo.value} value={tipo.value} text={tipo.name}
                            customStyles={typeSelected === tipo.value ? optionSelectedStyles : optionsStyles}
                            disabled={typeSelected === tipo.value} />
                    ))}
                </MenuOptions>
            </Menu>
        );
    };

    useEffect(() => { updateDocumentos() }, []);

    async function updateDocumentos() {
        const reservas = await getAllReservas();
        const guardias = await getAllGuardias();

        const eventos = reservas.concat(guardias);
        setListaEventos(eventos.sort((a, b) => a.fecha.diff(b.fecha) * -1));
    }

    function getAction(evento) {
        switch (evento.tipo) {
            case 'Reserva':
                navigation.navigate(ReservasScreenName, { datos: evento })
                break;
            case 'Guardia':
                setEventoSelected(evento);
                break;
        }
    }

    function fechaHoy() {
        return moment(new Date());
    }

    function canTouch(evento) {
        return evento.fecha.isAfter(fechaHoy(), 'day');
    }

    return (
        <View>
            <ScrollView>
                {listaEventos.map((evento) => (
                    <View key={evento.id} >
                        {
                            canTouch(evento) ?
                                <TouchableOpacity activeOpacity={0.8} onPress={() => getAction(evento)}>
                                    {(typeSelected === "All" || typeSelected === evento.tipo) &&
                                        <Evento tipo={evento.tipo} titulo={evento.clase} ubicacion={evento.aula} fecha={evento.fecha} color="green" />
                                    }
                                </TouchableOpacity>
                                :
                                (typeSelected === "All" || typeSelected === evento.tipo) &&
                                <Evento tipo={evento.tipo} titulo={evento.clase} ubicacion={evento.aula} fecha={evento.fecha} color="gray" />
                        }
                    </View>
                ))}
            </ScrollView>

            {eventoSelected != undefined &&
                <ModalResumenGuardia guardia={eventoSelected} setGuardia={setEventoSelected} />
            }
        </View>
    );
}

const optionsStyles = {
    optionText: {
        fontSize: 20,
        marginVertical: 5,
        textAlign: 'center'
    },
};

const optionSelectedStyles = {
    optionText: {
        fontSize: 20,
        marginVertical: 5,
        textAlign: 'center',
        color: 'green',
    },
};