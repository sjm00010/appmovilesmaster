import { Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';

import colors from '@styles/colors'

import { HomeScreen, HomeScreenName } from '@screens/HomeScreen';
import { ReservasScreen, ReservasScreenName } from '@screens/ReservasScreen';
import { GuardiasScreen, GuardiasScreenName } from '@screens/GuardiasScreen';

const Tab = createBottomTabNavigator();

function LogoTitle() {
  return (
    <Image
      style={{ width: 45, height: 48, marginLeft: 10 }}
      source={require('@assets/favicon.png')}
    />
  );
}

function MainContainer() {
  return (
    <Tab.Navigator
      initialRouteName={HomeScreenName}
      screenOptions={({ route }) => ({
        tabBarStyle: { height: 65, paddingBottom: 7, borderTopColor: colors.GRAY, borderTopWidth: 2 },
        headerLeft: (props) => <LogoTitle {...props} />,
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          const rn = route.name;

          switch (rn) {
            case HomeScreenName:
              iconName = focused ? 'home' : 'home-outline';
              break;
            case ReservasScreenName:
              iconName = focused ? 'md-school' : 'md-school-outline';
              break;
            case GuardiasScreenName:
              iconName = focused ? 'md-medkit' : 'md-medkit-outline';
              break;
            default:
              iconName = focused ? 'ios-bug' : 'ios-bug-outline';
          }

          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: colors.GREEN,
        tabBarInactiveTintColor: colors.BLACK,
      })}
    >

      <Tab.Screen name={HomeScreenName} component={HomeScreen} options={{ unmountOnBlur: true }} />
      <Tab.Screen name={ReservasScreenName} component={ReservasScreen} options={{ unmountOnBlur: true }} />
      <Tab.Screen name={GuardiasScreenName} component={GuardiasScreen} options={{ unmountOnBlur: true }} />

    </Tab.Navigator>
  );
}

export default MainContainer;