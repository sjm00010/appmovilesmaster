import { View, TextInput, Text, ScrollView, Button, StatusBar, Alert, TouchableOpacity, Image } from 'react-native';
import { SelectList } from 'react-native-dropdown-select-list';
import { useEffect, useState } from 'react';
import color from '@styles/colors';
import { styles } from '@styles/styles';
import { getAllClases } from '@storage/ClasesAsyncStorage';
import { saveBajas, getAllBajas } from '@storage/GuardiasAsyncStorage';
import CalendarPicker from 'react-native-calendar-picker';
import moment from 'moment';
import 'moment/locale/es';
import { reservasStyles } from '@styles/styles';
import uuid from 'react-native-uuid';
import { getClase } from '@storage/DataSimulator';

export const GuardiasScreenName = 'Bajas';

export function GuardiasScreen() {

    const [selectedFecha, setSelectedFecha] = useState(fechaHoy());
    const [selectedClaseId, setSelectedClaseId] = useState('');
    const [descripcion, setDescripcion] = useState('');

    const [showCalendarPicker, setShowCalendarPicker] = useState(false);
    const [showGuardiasForm, setShowGuardiasForm] = useState(false);
    const [editingBaja, setEditingBaja] = useState(undefined);

    const [listaClases, setListaClases] = useState([]);
    const [listaBajas, setListaBajas] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    async function fetchData() {
        setListaClases(await getAllClases());

        const bajas = await getAllBajas();
        setListaBajas(bajas.sort((a, b) => a.fecha.diff(b.fecha) * -1));
    }

    function isDayDisabled(date) {
        return isWeekend(date);
    }

    function isWeekend(date) {
        return date.day() == 0 || date.day() == 6;
    }
    function fechaHoy() {
        return moment(new Date());
    }

    function camposValidos() {
        if (selectedClaseId == '') {
            Alert.alert('Error en la clase', 'Por favor, seleccione una clase');
            return false;
        }

        return true;
    }

    function limpiarCampos() {
        setShowCalendarPicker(false);
        setShowGuardiasForm(false);
        setEditingBaja(undefined);
        setSelectedFecha(fechaHoy());
        setSelectedClaseId('');
        setDescripcion('');
    }
    async function addGuardia() {
        if (!camposValidos()) return;

        const baja = {
            id: editingBaja == undefined ? uuid.v4() : editingBaja.id,
            tipo: 'Guardia',
            fecha: selectedFecha,
            clase: selectedClaseId,
            descripcion
        }

        const bajas = await getAllBajas();

        const idxBaja = bajas.findIndex(b => b.id === baja.id);

        if (idxBaja === -1)
            bajas.push(baja);
        else
            bajas[idxBaja] = baja

        setListaBajas(bajas);
        await saveBajas(bajas);

        limpiarCampos();
    }

    async function deleteBajaHandler(bajaPos) {
        listaBajas.splice(bajaPos, 1);
        const newBajas = [...listaBajas];
        setListaBajas(newBajas);
        await saveBajas(newBajas);
    }

    function editBajaHandler(baja) {
        if (!canEditBaja(baja)) return;

        setSelectedFecha(baja.fecha);
        setSelectedClaseId(baja.clase);
        setDescripcion(baja.descripcion);

        setEditingBaja(baja);

        setShowGuardiasForm(true);
    }

    function canEditBaja(baja) {
        return baja.fecha.isAfter(fechaHoy(), 'day');
    }

    return (
        <ScrollView
            keyboardDismissMode='on-drag'
            keyboardShouldPersistTaps='always'
            style={{ backgroundColor: color.WHITE }}>
            <StatusBar backgroundColor={color.BLUE} translucent={true} />

            {
                showGuardiasForm ?
                    <View>
                        <View style={{ padding: 10, alignContent: 'center' }}>
                            <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Fecha</Text>

                            <Button
                                title="Seleccionar fecha"
                                onPress={() => setShowCalendarPicker(!showCalendarPicker)}
                            />
                            {showCalendarPicker ?
                                <View>
                                    <CalendarPicker
                                        onDateChange={setSelectedFecha}
                                        minDate={fechaHoy()}
                                        initialDate={selectedFecha}
                                        visible={showCalendarPicker}
                                        weekdays={['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']}
                                        months={[
                                            'Enero',
                                            'Febrero',
                                            'Marzo',
                                            'Abril',
                                            'Mayo',
                                            'Junio',
                                            'Julio',
                                            'Agosto',
                                            'Septiembre',
                                            'Octubre',
                                            'Noviembre',
                                            'Diciembre',
                                        ]}
                                        previousTitle="Anterior"
                                        nextTitle="Siguiente"
                                        startFromMonday
                                        disabledDates={isDayDisabled}
                                    />
                                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', marginTop: 20 }}>Fecha de la baja: {selectedFecha.format('DD-MM-YYYY')}</Text>
                                </View>
                                : null
                            }
                        </View>

                        <View style={{ padding: 10 }}>
                            <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Clase</Text>
                            <SelectList setSelected={setSelectedClaseId} data={listaClases.map(c => c.id)} defaultOption={{ key: selectedClaseId, value: selectedClaseId }} placeholder="Seleccione la clase" />
                        </View>

                        <View style={{ padding: 10 }}>
                            <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Descripción</Text>
                            <TextInput
                                label="Descripción"
                                placeholder="Introduzca unas instrucciones para el sustituto"
                                style={{ height: 200, borderColor: color.GRAY, borderWidth: 1, borderRadius: 5, padding: 10 }}
                                multiline
                                value={descripcion}
                                onChangeText={setDescripcion}
                                textAlignVertical='top'
                            />
                        </View>

                        <View style={{ padding: 10, alignContent: 'center' }}>
                            <Button
                                title={editingBaja == undefined ? "Crear baja" : "Modificar baja"}
                                style={reservasStyles.buttonReservas}
                                color={color.GREEN}
                                onPress={() => addGuardia()}
                            />
                        </View>

                        <View style={{ padding: 10, alignContent: 'center' }}>
                            <Button
                                title="Cancelar"
                                style={reservasStyles.buttonReservas}
                                color={color.RED}
                                onPress={() => limpiarCampos()}
                            />
                        </View>
                    </View>
                    :
                    <View>
                        <View style={{ padding: 10, alignContent: 'center' }}>
                            <Button
                                title="Crear nueva baja"
                                style={reservasStyles.buttonReservas}
                                onPress={() => setShowGuardiasForm(true)}
                            />
                        </View>

                        <View style={{ padding: 10, alignContent: 'center' }}>
                            <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Tus bajas</Text>
                            {
                                listaBajas.map((baja, idx) =>
                                    <View style={{ ...styles.inputContainer }} key={baja.id}>

                                        {
                                            canEditBaja(baja) ?
                                                <TouchableOpacity onPress={() => editBajaHandler(baja)} style={{ flex: 1 }}>
                                                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'left' }}>{getClase(baja.clase)}</Text>
                                                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'left' }}>{baja.fecha.format('YYYY-MM-DD')}</Text>
                                                </TouchableOpacity>
                                                :
                                                <View>
                                                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'left', color: 'gray' }}>{getClase(baja.clase)}</Text>
                                                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'left', color: 'gray' }}>{baja.fecha.format('YYYY-MM-DD')}</Text>
                                                </View>
                                        }
                                        {
                                            canEditBaja(baja) ?
                                                <View>
                                                    <TouchableOpacity onPress={() => deleteBajaHandler(idx)} style={{ padding: 15 }}>
                                                        <Image
                                                            style={reservasStyles.buttonImageIconStyle}
                                                            source={require('@assets/delete.jpg')}
                                                        />
                                                    </TouchableOpacity>
                                                </View>
                                                : null
                                        }
                                    </View>
                                )
                            }
                        </View>
                    </View>
            }
        </ScrollView>
    );
}