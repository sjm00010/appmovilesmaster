import { useState } from 'react';
import {
    View,
    TouchableOpacity,
    StatusBar,
    Image,
    ScrollView,
    Alert
} from 'react-native';
import { mainStyles, styles } from '@styles/styles';
import MyButton from '@components/MyButton';
import color from '@styles/colors';
import { Input, Text } from 'react-native-elements';
import { isUserValid } from '@storage/UsuarioAsyncStorage';
import { getUser } from '@storage/DataSimulator';

export default function LoginScreen(props) {

    const [email, setEmail] = useState(getUser().email);
    const [password, setPassword] = useState(getUser().password);
    const [isSecureEntry, setIsSecureEntry] = useState(true);

    return (
        <ScrollView
            keyboardDismissMode='on-drag'
            keyboardShouldPersistTaps='always'
            style={{ backgroundColor: color.WHITE }}>

            <View style={[mainStyles.container, { padding: 10 }]}>
                <StatusBar backgroundColor={color.BLUE} translucent={true} />
                <View style={styles.logo}>
                    <Image source={require('@assets/uja.png')}
                        style={{ height: 150, width: 150 }} />
                </View>
                <Text>Para la demo las credenciales de acceso son: </Text>
                <Text selectable={true} style={{ color: 'red' }}>{email}</Text>
                <Text selectable={true} style={{ color: 'red' }}>{password}</Text>
                <Input
                    label="E-mail"
                    placeholder="Introduzca un E-mail"
                    style={{ alignItems: 'center' }}
                    value={email}
                    onChangeText={setEmail}
                    inputStyle={{
                        fontSize: 18, paddingVertical: 10,
                        paddingHorizontal: 8, marginTop: 12,
                        color: color.PRIMARYCOLOR,
                        fontFamily: "normal",
                        borderBottomColor: color.PRIMARYCOLOR
                    }}
                    placeholderTextColor={color.LIGHTPRIMARYCOLOR}
                />
                <Input
                    label="Contraseña"
                    placeholder="Introduzca la contraseña"
                    secureTextEntry={isSecureEntry}
                    style={{ alignItems: 'center' }}
                    value={password}
                    onChangeText={setPassword}
                    inputStyle={{
                        fontSize: 18, paddingVertical: 10,
                        paddingHorizontal: 8, marginTop: 12,
                        color: color.PRIMARYCOLOR,
                        fontFamily: "normal",
                        borderBottomColor: color.PRIMARYCOLOR
                    }}
                    placeholderTextColor={color.LIGHTPRIMARYCOLOR}
                    rightIcon={<TouchableOpacity activeOpacity={0.8} style={styles.btnVisibility} onPress={() => {
                        setIsSecureEntry((isSecureEntry) => !isSecureEntry);
                    }}>
                        <Image style={styles.btnImage} tintColor={color.PRIMARYCOLOR}
                            source={isSecureEntry ? require('@assets/ic_show_password.png') : require('@assets/ic_hide_password.png')} />
                    </TouchableOpacity>}
                />

                <MyButton
                    titulo='Iniciar Sesión'
                    onPress={() =>
                        iniciarSesion()
                    }
                />
            </View>
        </ScrollView>

    )

    async function iniciarSesion() {
        if (!areFieldsValid()) return;

        if (isUserValid({ email, password }))
            goToScreen('MainContainer');
        else
            Alert.alert("Credenciales inválidas", "Email o contraseña incorrectos.");
    }

    function areFieldsValid() {
        const validator = require("email-validator");
        if (!validator.validate(email)) {
            Alert.alert("Error en E-mail", "El correo electrónico no es válido")
            return false;
        }

        if (password === '') {
            Alert.alert("Error en la contraseña", "La contraseña no puede ser vacía")
            return false;
        }

        return true
    }

    function goToScreen(routeName) {
        props.navigation.navigate(routeName)
    }

}