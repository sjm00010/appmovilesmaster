import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator } from '@react-navigation/native-stack';
import { MenuProvider } from 'react-native-popup-menu';

import LoginScreen from '@screens/LoginScreen';
import MainContainer from './screens/MainContainer';
import { UsuarioProvider } from '@context/UsuarioContext';

import { setupDummyData } from '@storage/DataSimulator';
 
const Stack = createNativeStackNavigator();

setupDummyData();

function MyStack(){
  return(
    <Stack.Navigator> 
      <Stack.Screen name="Login" component = {LoginScreen}/>
      <Stack.Screen name="MainContainer" component = {MainContainer}  
              options={{
                  headerShown: false
                }} 
      />
    </Stack.Navigator>
    
  )
}

export default function App() {
  return (
    <MenuProvider>
      <NavigationContainer>
        <UsuarioProvider>
          <MyStack />
        </UsuarioProvider>
      </NavigationContainer>
    </MenuProvider>
  );
}
